package com.nlmk.craftteam.aggregator.controller;

import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import com.nlmk.craftteam.aggregator.service.CarAggregateService;
import com.nlmk.craftteam.aggregator.service.CarBrandService;
import com.nlmk.craftteam.aggregator.service.CarModelService;
import com.nlmk.craftteam.aggregator.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/")
public class AggregatorControllerImpl implements  AggregatorController{

    @Value("#{'${car.hosts}'.split(',')}")
    private List<String> hosts;

    private CarService carService;

    private CarAggregateService carAggregateService;

    private CarModelService carModelService;

    private CarBrandService carBrandService;

    @Autowired
    public AggregatorControllerImpl(CarService carService, CarAggregateService carAggregateService, CarModelService carModelService, CarBrandService carBrandService) {
        this.carService = carService;
        this.carAggregateService = carAggregateService;
        this.carModelService = carModelService;
        this.carBrandService = carBrandService;
    }

    @Override
    @GetMapping("/aggregator")
    public ModelAndView getAllBrandsAndModels() {
        ModelAndView model = new ModelAndView();
        model.setViewName("aggregator");

        ArrayList<CarAggregate> aggregateData = new ArrayList<>();

        for (String host : hosts) {
            ResponseEntity<CarBrand[]> brands = new RestTemplate().getForEntity(host + "/brands", CarBrand[].class);
            List<CarBrand> carBrands = carBrandService.save(brands.getBody());

            for (CarBrand carBrand : carBrands) {
                ResponseEntity<CarModel[]> models = new RestTemplate().getForEntity(host + "/models/{brandId}", CarModel[].class, carBrand.getId());
                if (models.getBody() != null) {
                    List<CarModel> modelsBody = Arrays.stream(models.getBody()).map(mod ->
                            CarModel.builder().carBrand(carBrand).name(mod.getName()).build()
                    ).collect(Collectors.toList());
                    List<CarAggregate> carAggregates = carAggregateService.save(modelsBody, host);
                    aggregateData.addAll(carAggregates);
                }
            }

        }
        model.addObject("data", aggregateData);
        return model;
    }
}
