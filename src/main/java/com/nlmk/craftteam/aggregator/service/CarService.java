package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.CarAggregateRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Data
@Transactional
public class CarService {

    private CarAggregateRepository carAggregateRepository;

    @Autowired
    public CarService(CarAggregateRepository carAggregateRepository) {
        this.carAggregateRepository = carAggregateRepository;
    }

    public CarBrand[] save(CarBrand[] body) {
        List<CarAggregate> all = carAggregateRepository.findAll();
        return new CarBrand[0];
    }
}
