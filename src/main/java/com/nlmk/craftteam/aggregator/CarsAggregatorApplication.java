package com.nlmk.craftteam.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "com.nlmk.craftteam.aggregator")
public class CarsAggregatorApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CarsAggregatorApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(CarsAggregatorApplication.class, args);
	}

}

