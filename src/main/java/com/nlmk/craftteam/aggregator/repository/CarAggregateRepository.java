package com.nlmk.craftteam.aggregator.repository;

import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarAggregateRepository extends JpaRepository<CarAggregate, Long> {

}
