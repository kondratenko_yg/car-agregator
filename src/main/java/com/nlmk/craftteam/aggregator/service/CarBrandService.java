package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.CarBrandRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarBrand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarBrandService {

    private CarBrandRepository carBrandRepository;

    @Autowired
    public CarBrandService(CarBrandRepository carBrandRepository) {
        this.carBrandRepository = carBrandRepository;
    }

    public List<CarBrand> save(CarBrand[] body) {
        return carBrandRepository.saveAll(Arrays.asList(body).stream().map(c -> CarBrand.builder()
                .name(c.getName())
                .build()).collect(Collectors.toList()));
    }
}
