package com.nlmk.craftteam.aggregator.controller;

import org.springframework.web.servlet.ModelAndView;

public interface AggregatorController {
    ModelAndView getAllBrandsAndModels();
}
