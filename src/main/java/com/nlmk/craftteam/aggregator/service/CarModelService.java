package com.nlmk.craftteam.aggregator.service;


import com.nlmk.craftteam.aggregator.repository.CarModelRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class CarModelService {

    private CarModelRepository carModelRepository;

    @Autowired
    public CarModelService(CarModelRepository carModelRepository) {
        this.carModelRepository = carModelRepository;
    }

    public List<CarModel> save(CarModel[] body) {

        return carModelRepository.saveAll(Arrays.asList(body));
    }

}
