package com.nlmk.craftteam.aggregator.service;

import com.nlmk.craftteam.aggregator.repository.CarAggregateRepository;
import com.nlmk.craftteam.aggregator.repository.entity.CarAggregate;
import com.nlmk.craftteam.aggregator.repository.entity.CarModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CarAggregateService {

    private CarAggregateRepository carAggregateRepository;

    @Autowired
    public CarAggregateService(CarAggregateRepository carAggregateRepository) {
        this.carAggregateRepository = carAggregateRepository;
    }

    public List<CarAggregate> save(List<CarModel> modelsBody, String host) {
        if (host == null || host.isEmpty()) return new ArrayList<>();
        List<CarAggregate> carAggregates = modelsBody.stream()
                .map(body -> CarAggregate.builder()
                        .carModel(body)
                        .host(host)
                        .build()
                ).collect(Collectors.toList());
        return carAggregateRepository.saveAll(carAggregates);
    }
}
